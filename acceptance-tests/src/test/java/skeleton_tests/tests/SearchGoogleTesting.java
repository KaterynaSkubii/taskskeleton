package skeleton_tests.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.Test;
import skeleton_tests.data.Data;
import work.rustam.common.services.ui.pages.GoogleStartPage;

import java.util.List;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.page;

public class SearchGoogleTesting extends BaseTest {

    @Value("${ui.baseUrl}")
    String baseUrl;

    //TODO: INFO х2 (при параллелизации)
    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: Google Search. Search Action")
    @Step("#4 verify first element from search results")
    @Test(groups = "ui", dataProvider = "searchResultsReturnedTest", dataProviderClass = Data.class, timeOut = 60000)
    public void searchResultsReturnedTest(String searchWord, String expectedText) {
        openUrl()
                .search(searchWord)
                .firstResult()
                .shouldHave(text(expectedText));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: Google Search. Verify Footer Element Names")
    @Step("#4 verify that footer contains elements: \"Help\", \"Send feedback\", \"Privacy\", \"Terms\"")
    @Test(groups = "ui", dataProvider = "verifyFooterElementNamesTest", dataProviderClass = Data.class, timeOut = 60000)
    public void verifyFooterElementNamesTest(String searchWord, List<String> footerElementsNames) {
        openUrl()
                .search(searchWord)
                .navigationLabels()
                .shouldHave(texts(footerElementsNames));
    }

    @Step("#1 I open Google Page")
    protected GoogleStartPage openUrl() {
        open(baseUrl, GoogleStartPage.class);
        return page(GoogleStartPage.class);
    }
}
