package skeleton_tests.tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Ordering;
import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;
import org.testng.annotations.Test;
import skeleton_tests.data.Data;
import skeleton_tests.models.ListBreweries;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.springframework.util.StringUtils.hasText;
import static skeleton_tests.data.Path.CITY;
import static skeleton_tests.data.Path.ID;
import static skeleton_tests.data.Path.ID_ASC;
import static skeleton_tests.data.Path.NAME;
import static skeleton_tests.data.Path.POSTAL;
import static skeleton_tests.data.Path.STATE;
import static skeleton_tests.data.Path.SIZE;
import static skeleton_tests.data.Path.TYPE;
import static skeleton_tests.data.Path.TYPE_DESC;

public class BreweriesApiTesting extends BaseTest {

    @Value("${breweries.path.baseUrl}")
    String baseEndpointBrewery;

    @Value("${breweries.path.search.byName}")
    String searchBreweriesEndpoint;

    @Value("${breweries.path.filter.byCity}")
    String filterByCityEndpoint;

    @Value("${breweries.path.filter.byName}")
    String filterByNameEndpoint;

    @Value("${breweries.path.filter.byState}")
    String filterByStateEndpoint;

    @Value("${breweries.path.filter.byPostal}")
    String filterByPostalEndpoint;

    @Value("${breweries.path.filter.byType}")
    String filterByTypeEndpoint;

    @Value("${breweries.path.filter.byPage}")
    String filterByPageEndpoint;

    @Value("${breweries.path.filter.perPage}")
    String filterPerPageEndpoint;

    @Value("${breweries.path.sort}")
    String sortEndpoint;

    ObjectMapper objectMapper = new ObjectMapper();

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: search breweries")
    @Step("#2 response contains breweries which have search string")
    @Test(groups = "breweries", dataProvider = "breweriesFilterByCityTest", dataProviderClass = Data.class, timeOut = 10000)
    public void searchBreweriesTest(String name, String symbol, String replacement) {
        List<String> listOfBreweriesName = restService
                .sendGetRequest(baseEndpointBrewery + searchBreweriesEndpoint + keyWord(name, symbol, replacement))
                .body().jsonPath().get(NAME.getText());
        listOfBreweriesName.forEach(n -> assertThat(n, hasText(n)));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: filtering breweries by city")
    @Step("#2 response contains breweries which are located in the city specified in the url")
    @Test(groups = "breweries", dataProvider = "breweriesFilterByCityTest", dataProviderClass = Data.class, timeOut = 10000)
    public void breweriesFilterByCityTest(String city, String symbol, String replacement) {
        restService
                .sendGetRequest(baseEndpointBrewery + filterByCityEndpoint + keyWord(city, symbol, replacement))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(CITY.getText(), everyItem(equalTo(city)));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: filtering breweries by name")
    @Step("#2 response contains breweries which have name specified in the url")
    @Test(groups = "breweries", dataProvider = "breweriesFilterByNameTest", dataProviderClass = Data.class, timeOut = 10000)
    public void breweriesFilterByNameTest(String name, String symbol, String replacement) {
        restService
                .sendGetRequest(baseEndpointBrewery + filterByNameEndpoint + keyWord(name, symbol, replacement))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(NAME.getText(), everyItem(equalTo(name)));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: filtering breweries by state")
    @Step("#2 response contains breweries which are located in the state specified in the url")
    @Test(groups = "breweries", dataProvider = "breweriesFilterByStateTest", dataProviderClass = Data.class, timeOut = 10000)
    public void breweriesFilterByStateTest(String state, String symbol, String replacement) {
        restService
                .sendGetRequest(baseEndpointBrewery + filterByStateEndpoint + keyWord(state, symbol, replacement))
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(STATE.getText(), everyItem(equalTo(state)));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: filtering breweries by postal")
    @Step("#2 response contain breweries which have postal specified in the url")
    @Test(groups = "breweries", dataProvider = "breweriesFilterByPostalTest", dataProviderClass = Data.class, timeOut = 10000)
    public void breweriesFilterByPostalTest(String postal) {
        restService
                .sendGetRequest(baseEndpointBrewery + filterByPostalEndpoint + postal)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(POSTAL.getText(), everyItem(equalTo(postal)));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: filtering breweries by type")
    @Step("#2 response contains breweries which have type specified in the url")
    @Test(groups = "breweries", dataProvider = "breweriesFilterByTypeTest", dataProviderClass = Data.class, timeOut = 10000)
    public void breweriesFilterByTypeTest(String type) {
        restService
                .sendGetRequest(baseEndpointBrewery + filterByTypeEndpoint + type)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(TYPE.getText(), everyItem(equalTo(type)));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: filtering breweries by page")
    @Step("#2 response contains breweries which are different from the list on the previous page")
    @Test(groups = "breweries", dataProvider = "breweriesFilterByPageTest", dataProviderClass = Data.class, timeOut = 10000)
    public void breweriesFilterByPageTest(int page) {
        int previousPage = page - 1;
        List<Integer> listOfObjectsIdFromPreviousPage
                = restService.sendGetRequest(baseEndpointBrewery + filterByPageEndpoint + previousPage)
                .body().jsonPath().get(ID.getText());
        List<Integer> listCurrent
                = restService.sendGetRequest(baseEndpointBrewery + filterByPageEndpoint + page)
                .body().jsonPath().get(ID.getText());
        assertThat(listOfObjectsIdFromPreviousPage, not(containsInAnyOrder(listCurrent)));
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: filtering breweries per page")
    @Step("#2 response contains count of breweries which specified in the url")
    @Test(groups = "breweries", dataProvider = "breweriesFilterPerPageTest", dataProviderClass = Data.class, timeOut = 10000)
    public void breweriesFilterPerPageTest(int numberOfItemsPerPage) {
        restService
                .sendGetRequest(baseEndpointBrewery + filterPerPageEndpoint + numberOfItemsPerPage)
                .then().statusCode(HttpStatus.SC_OK)
                .assertThat().body(SIZE.getText(), is(numberOfItemsPerPage));
    }

    @SneakyThrows
    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: sorting breweries")
    @Step("#2 response contains breweries which sorted by two fields")
    @Test(groups = "breweries", dataProvider = "sortBreweriesTest", dataProviderClass = Data.class, timeOut = 10000)
    public void sortBreweriesTest(String city, String symbol, String replacement) {
        Response response = restService.sendGetRequest(baseEndpointBrewery + filterByCityEndpoint
                + keyWord(city, symbol, replacement) + sortEndpoint + TYPE_DESC.getText() + "," + ID_ASC.getText());

        ListBreweries[] listBreweries = objectMapper.readValue(response.asString(), ListBreweries[].class);

        List<String> listOfBreweriesTypes = Arrays.stream(listBreweries)
                .map(ListBreweries::getBreweryType)
                .collect(Collectors.toList());

        List<Integer> listOfBreweriesId = Arrays.stream(listBreweries)
                .map(ListBreweries::getId)
                .collect(Collectors.toList());

        assertThat(Ordering.natural().isOrdered(listOfBreweriesTypes), equalTo(true));
        assertThat(Ordering.natural().reverse().isOrdered(listOfBreweriesId), equalTo(true));
    }

    public String keyWord(String text, String symbol, String replacement) {
        return text.toLowerCase().replace(symbol, replacement);
    }
}
