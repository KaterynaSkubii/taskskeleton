package skeleton_tests.tests;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Value;

import org.testng.annotations.Test;
import skeleton_tests.data.Data;

import static org.hamcrest.Matchers.is;

public class UserManagementApiTesting extends BaseTest {

    @Value("${reqres.path.baseUrl}")
    String baseEndpoint;

    @Value("${reqres.path.get.user.url}")
    String userInfoEndpoint;

    @Value("${reqres.path.get.allUsers.url}")
    String allUsersEndpoint;

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: Users management. Get single user")
    @Step("#2 response contain user with first name equal to 'fuchsia rose'")
    @Test(groups = "api", dataProvider = "getSingleUserTest", dataProviderClass = Data.class, timeOut = 5000)
    public void getSingleUserTest(int id, String name) {
        restService
                .sendGetRequest(baseEndpoint + userInfoEndpoint + id).then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("data.name", is(name));

        //TODO: or
//        restService
//                .sendGetRequest(baseEndpoint + userInfoEndpoint + id).then().statusCode(HttpStatus.SC_OK)
//                .assertThat().extract().as(UserResponse.class).getData().getName()
//                .equals(name);
    }

    @Severity(SeverityLevel.NORMAL)
    @Description("Test Description: Users management. Get all users")
    @Step("#2 response contain data for 6 users")
    @Test(groups = "api", dataProvider = "getAllUsersTest", dataProviderClass = Data.class, timeOut = 5000)
    public void getAllUsersTest(int numberOfUsers) {
        restService
                .sendGetRequest(baseEndpoint + allUsersEndpoint).then().statusCode(HttpStatus.SC_OK)
                .assertThat().body("size()", is(numberOfUsers));
    }
}
