package skeleton_tests.tests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import skeleton_tests.configuration.AppConfig;
import skeleton_tests.configuration.BaseTestListener;
import work.rustam.common.services.api.RestClient;

import static com.codeborne.selenide.WebDriverRunner.closeWebDriver;
import static com.codeborne.selenide.WebDriverRunner.isIE;
import static work.rustam.common.services.ui.drivers.DriverManager.initBrowser;

@Test
@Listeners({BaseTestListener.class})
public abstract class BaseTest extends AppConfig {

    @Autowired
    @Lazy
    protected RestClient restService;

    @Value("${webdriver.browser}")
    private String browser;


    @BeforeMethod
    public void beforeMethod() {
        initBrowser(browser);
    }

    @AfterMethod
    public static void ieRelax() {
        if (isIE()) {
            closeWebDriver();
        }
    }
}

//    mvn clean test
//    allure serve acceptance-tests/target/allure-results

// mvn test -PallDev
// mvn test -PuiDev
// mvn test -PapiDev
// mvn test -PbreweriesDev

// mvn test -PallQa
// mvn test -PuiQa
// mvn test -PapiQa
// mvn test -PbreweriesQa
