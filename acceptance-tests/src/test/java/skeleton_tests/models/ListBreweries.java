package skeleton_tests.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Builder;
import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
		"country",
		"brewery_type",
		"city",
		"address_2",
		"latitude",
		"address_3",
		"created_at",
		"website_url",
		"updated_at",
		"phone",
		"street",
		"name",
		"county_province",
		"id",
		"state",
		"postal_code",
		"longitude"
})

@Data
@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public class ListBreweries {

	@JsonProperty("country")
	private String country;

	@JsonProperty("brewery_type")
	private String breweryType;

	@JsonProperty("city")
	private String city;

	@JsonProperty("address_2")
	private Object address2;

	@JsonProperty("latitude")
	private String latitude;

	@JsonProperty("address_3")
	private Object address3;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("website_url")
	private String websiteUrl;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("street")
	private String street;

	@JsonProperty("name")
	private String name;

	@JsonProperty("county_province")
	private Object countyProvince;

	@JsonProperty("id")
	private int id;

	@JsonProperty("state")
	private String state;

	@JsonProperty("postal_code")
	private String postalCode;

	@JsonProperty("longitude")
	private String longitude;

	@Override
 	public String toString(){
		return
				"ListBreweries{\n" +
						"\t\"country\": \"" + country + "\",\n" +
						"\t\"brewery_type\": \"" + breweryType + "\",\n" +
						"\t\"city\": " + city + ",\n" +
						"\t\"address_2\": " + address2 + ",\n" +
						"\t\"latitude\": \"" + latitude + "\",\n" +
						"\t\"address_3\": \"" + address3 + "\",\n" +
						"\t\"created_at\": " + createdAt + ",\n" +
						"\t\"website_url\": " + websiteUrl + ",\n" +
						"\t\"updated_at\": \"" + updatedAt + "\",\n" +
						"\t\"phone\": \"" + phone + "\",\n" +
						"\t\"street\": " + street + ",\n" +
						"\t\"name\": " + name + ",\n" +
						"\t\"county_province\": \"" + countyProvince + "\",\n" +
						"\t\"id\": \"" + id + "\",\n" +
						"\t\"state\": " + state + ",\n" +
						"\t\"postal_code\": " + postalCode + ",\n" +
						"\t\"longitude\": " + longitude + "\n" +
						"}";
		}
}