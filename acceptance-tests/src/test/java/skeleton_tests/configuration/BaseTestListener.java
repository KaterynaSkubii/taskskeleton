package skeleton_tests.configuration;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Attachment;
import io.qameta.allure.selenide.AllureSelenide;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import skeleton_tests.data.Data;
import work.rustam.common.utils.session.Key;
import work.rustam.common.utils.session.Session;

import static com.codeborne.selenide.Configuration.browser;

@Slf4j
public class BaseTestListener extends Data implements ISuiteListener, ITestListener {

    @Override
    public void onStart(final ISuite suite) {
        log.info(SUITE, suite.getName());
        log.info(testStatusText.getTEXT_FOR_START());
        setSuiteName(suite);
        SelenideLogger.addListener(ALLURE, new AllureSelenide());
        Session.getCurrentSession().put(Key.Keys.ACTIVE_BROWSER, browser);
    }

    @Override
    public void onStart(ITestContext TestContext) {
    }

    @Override
    public void onTestStart(ITestResult testResult) {
        count++;
        log.info(TEST_CASE_STARTED, testResult.getName());
    }

    @Override
    public void onTestSuccess(ITestResult testResult) {
        passedTests++;
        log.info(NAME_OF_THE_TEST, PASSED, testResult.getName());
        log.info(testStatusText.getTEXT_FOR_PASSED_TEST());
    }

    @Override
    public void onTestFailure(ITestResult testResult) {
        failedTests++;
        log.info(NAME_OF_THE_TEST, FAILED, testResult.getName());
        //TODO: валиден ли такой вариант (try-with)? - try(AutoCloseable ignored = WebDriverRunner::closeWebDriver) {
        try(AutoCloseable ignored = WebDriverRunner::closeWebDriver) {
            screenshot();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info(testStatusText.getTEXT_FOR_FAILED_TEST());
    }

    @Override
    public void onTestSkipped(ITestResult testResult) {
        skippedTests++;
        log.info(NAME_OF_THE_TEST, SKIPPED, testResult.getName());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult testResult) {
        failedButWithinSuccessPercentageTests++;
        log.info(NAME_OF_THE_TEST, FAILED_BUT, testResult.getName());;
    }

    @Override
    public void onFinish(ITestContext context) {
    }

    @Override
    public void onFinish(ISuite suite) {
        log.info(TESTS_COMPLETED, count);
        Session.getCurrentSession().put(Key.Keys.COUNT, String.valueOf(count));
        log.info(RESULT, passedTests, failedTests, skippedTests, failedButWithinSuccessPercentageTests);
        Session.getCurrentSession().put(PASSED, String.valueOf(passedTests));
        Session.getCurrentSession().put(FAILED, String.valueOf(failedTests));
        Session.getCurrentSession().put(SKIPPED, String.valueOf(skippedTests));
        Session.getCurrentSession().put(FAILED_BUT,
                String.valueOf(failedButWithinSuccessPercentageTests));
        log.info(TEST_SUITE_COMPLETED);

    }

    // TODO: не выходят читабельные скрины
    @Attachment(value = VALUE_ATTACHMENT, type = TYPE_ATTACHMENT)
    public void screenshot() {
        Configuration.reportsFolder = PATH_NAME;
    }

    public void setSuiteName(ISuite suite) {
        MDC.put(KEY_SUITE_NAME, suite.getName());
    }
}
