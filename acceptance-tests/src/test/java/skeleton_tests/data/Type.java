package skeleton_tests.data;

import lombok.Getter;

@Getter
public enum Type {
    MICRO("micro"),
    NANO("nano"),
    REGIONAL("regional"),
    BREWPUB("brewpub"),
    LARGE("large"),
    PLANNING("planning"),
    BAR("bar"),
    CONTRACT("contract"),
    PROPRIETOR("proprietor"),
    CLOSED("closed");

    private final String text;

    Type(final String text) {
        this.text = text;
    }
}
