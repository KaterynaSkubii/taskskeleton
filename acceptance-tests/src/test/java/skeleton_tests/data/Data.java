package skeleton_tests.data;

import org.testng.annotations.DataProvider;
import work.rustam.common.utils.TestStatusText;

import java.util.Arrays;
import java.util.List;

public class Data {

    // skeleton_tests/configuration/BaseTestListener.java
    protected static final TestStatusText testStatusText = new TestStatusText();
    protected static int failedTests = 0;
    protected static int passedTests = 0;
    protected static int count = 0;
    protected static int skippedTests = 0;
    protected static int failedButWithinSuccessPercentageTests = 0;
    protected static final String SUITE = "Suite: {}";
    protected static final String ALLURE = "AllureSelenide";
    protected static final String TEST_CASE_STARTED = "{} test case started";
    protected static final String NAME_OF_THE_TEST = "The name of the testcase {} is: {}";
    protected static final String TESTS_COMPLETED = "There are {} tests completed.";
    protected static final String RESULT
            = "Passed tests: {}, Failed test: {}, Skipped test: {}, Failed but within success percentage test: {}";
    protected static final String VALUE_ATTACHMENT = "Screenshot on fail";
    protected static final String TYPE_ATTACHMENT = "image/png";
    protected static final String PATH_NAME = "acceptance-tests/target/allure-results/screenshots";
    protected static final String KEY_SUITE_NAME = "suiteName";
    protected final String TEST_SUITE_COMPLETED = "\n\n\n\n\n\n " +
            "***************************************Test Suite Completed!!!!******************************************";
    protected static final String PASSED = "PASSED";
    protected static final String FAILED = "FAILED";
    protected static final String SKIPPED = "SKIPPED";
    protected static final String FAILED_BUT = "FAILED_BUT_within_success_percentage_tests";

    // skeleton_tests.tests.SearchGoogleTesting.java
    private static final String SEARCH_WORD = "selenide";
    private static final String EXPECTED_TEXT = "Selenide:";
    private static final List<String> FOOTER_ELEMENTS_NAMES = Arrays.asList("Help", "Send feedback", "Privacy", "Terms");

    // skeleton_tests.tests.UserManagementApiTesting.java
    private static final int ID = 2;
    private static final String NAME_FOR_GET_SINGLE_USER_TEST = "fuchsia rose";
    private static final int NUMBER_OF_USERS = 6;

    // skeleton_tests.tests.BreweriesApiTesting.java
    private static final String SEARCH_NAME = "Dog";
    private static final String CITY = "Alameda";
    private static final String NAME_FOR_BREWERIES_FILTER_TEST = "Almanac Beer Company";
    private static final String STATE = "California";
    private static final String POSTAL = "94501-5047";
    private static final int PAGE = 15;
    private static final int NUMBER_OF_ITEMS_PER_PAGE = 25;
    private static final String CITY_FOR_SORTING = "Orlando";
    private static final String SYMBOL = " ";
    private static final String REPLACEMENT = "_";

    // skeleton_tests.tests.SearchGoogleTesting.java
    @DataProvider(name = "searchResultsReturnedTest")
    public Object[][] combinedDataForSearchResultsReturnedTest() {
        return new Object[][]{
                {SEARCH_WORD, EXPECTED_TEXT}
        };
    }

    @DataProvider(name = "verifyFooterElementNamesTest")
    public Object[][] combinedDataForVerifyFooterElementNamesTest() {
        return new Object[][]{
                {SEARCH_WORD, FOOTER_ELEMENTS_NAMES}
        };
    }

    // skeleton_tests.tests.UserManagementApiTesting.java
    @DataProvider(name = "getSingleUserTest")
    public Object[][] combinedDataForGetSingleUserTest() {
        return new Object[][]{
                {ID, NAME_FOR_GET_SINGLE_USER_TEST}
        };
    }

    @DataProvider(name = "getAllUsersTest")
    public Object[][] combinedDataForGetAllUsersTest() {
        return new Object[][]{
                {NUMBER_OF_USERS}
        };
    }

    // skeleton_tests.tests.BreweriesApiTesting.java
    @DataProvider(name = "searchBreweriesTest")
    public Object[][] combinedDataForSearchBreweriesTest() {
        return new Object[][]{
                {SEARCH_NAME, SYMBOL, REPLACEMENT}
        };
    }

    @DataProvider(name = "breweriesFilterByCityTest")
    public Object[][] combinedDataForBreweriesFilterByCityTest() {
        return new Object[][]{
                {CITY, SYMBOL, REPLACEMENT}
        };
    }

    @DataProvider(name = "breweriesFilterByNameTest")
    public Object[][] combinedDataForBreweriesFilterByNameTest() {
        return new Object[][]{
                {NAME_FOR_BREWERIES_FILTER_TEST, SYMBOL, REPLACEMENT}
        };
    }

    @DataProvider(name = "breweriesFilterByStateTest")
    public Object[][] combinedDataForBreweriesFilterByStateTest() {
        return new Object[][]{
                {STATE, SYMBOL, REPLACEMENT}
        };
    }

    @DataProvider(name = "breweriesFilterByPostalTest")
    public Object[][] combinedDataForBreweriesFilterByPostalTest() {
        return new Object[][]{
                {POSTAL}
        };
    }

    @DataProvider(name = "breweriesFilterByTypeTest")
    public Object[][] combinedDataForBreweriesFilterByTypeTest() {
        return new Object[][]{
                {Type.MICRO.getText()}
        };
    }

    @DataProvider(name = "breweriesFilterByPageTest")
    public Object[][] combinedDataForBreweriesFilterByPageTest() {
        return new Object[][]{
                {PAGE}
        };
    }

    @DataProvider(name = "breweriesFilterPerPageTest")
    public Object[][] combinedDataForBreweriesFilterPerPageTest() {
        return new Object[][]{
                {NUMBER_OF_ITEMS_PER_PAGE}
        };
    }

    @DataProvider(name = "sortBreweriesTest")
    public Object[][] combinedDataForSortBreweriesTest() {
        return new Object[][]{
                {CITY_FOR_SORTING, SYMBOL, REPLACEMENT}
        };
    }
}
