package skeleton_tests.data;

import lombok.Getter;

@Getter
public enum Path {
    CITY("city"),
    NAME("name"),
    STATE("state"),
    POSTAL("postal_code"),
    TYPE("brewery_type"),
    ID("id"),
    SIZE("size()"),
    CITY_DESC("+city"),
    NAME_DESC("+name"),
    STATE_DESC("+state"),
    POSTAL_DESC("+postal_code"),
    TYPE_DESC("+brewery_type"),
    ID_DESC("-id"),
    CITY_ASC("-city"),
    NAME_ASC("-name"),
    STATE_ASC("-state"),
    POSTAL_ASC("-postal_code"),
    TYPE_ASC("-brewery_type"),
    ID_ASC("-id");

    private final String text;

    Path(final String text) {
        this.text = text;
    }
}
