This maven project contains two submodules: common/core module of test automation framework and acceptance test module

### Table of contents
1. [Required Software and Tools & Prerequisites](#required-software-and-tools)
2. [How to build maven projects and run all tests](#how-to-run-acceptance-tests)
3. [Reports](#reports)

<a name="required-software-and-tools"></a>
### Required Software and Tools & Prerequisites

* **Java** version: **Oracle Java 11** and higher (Execute `java -version` in command line after installation)
* **Apache Maven** version: **3.8.1** and higher (Execute `mvn -version` in command line after installation)

 <a name="how-to-run-acceptance-tests"></a>
### How to build maven projects and run all tests 

* Open a terminal or command prompt
* Go to project's root
* Execute `mvn clean install -PallDev` (or `mvn clean install`) for all suites tests (environment = dev)
       or `mvn clean install -PallQa"`for all suites tests (environment = qa)
* Execute `mvn clean install -PuiDev"` for UI tests (environment = dev),
       or `mvn clean install -PuiQa"`for UI tests (environment = qa)
* Execute `mvn clean install -PapiDev"` for API tests (environment = dev), 
       or `mvn clean install -PapiQa"` for API tests (environment = qa)
* Execute `mvn clean install -PbreweriesDev"` for breweries API tests (environment = dev),
       or `mvn clean install -PbreweriesQa"` for breweries API tests (environment = qa)
* In order to run tests in parallel mode set dataproviderthreadcount to value > 1 for maven-failsafe-plugin properties in pom.xml

<a name="reports"></a>
### Reports  

* **Allure** html test report should be available under `allure serve acceptance-tests/target/allure-results` after test execution